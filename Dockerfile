# syntax=docker/dockerfile:1
FROM python:3.6
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
WORKDIR /code
COPY requirements.txt /code/
RUN pip install -r requirements.txt
COPY . /code/
RUN apt-get update
RUN apt-get install -y nodejs npm
RUN npm install webpack
CMD python manage.py migrate
CMD python manage.py createsuperuser --noinput --username admin --email admin@admin.com
CMD npx webpack --progress

