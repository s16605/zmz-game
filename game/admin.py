from django.contrib import admin
from .models import Game, User, Group, Card
# Register your models here.

admin.site.register(Game)
admin.site.register(Card)
